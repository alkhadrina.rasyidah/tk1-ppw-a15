from django import forms
from tinymce.widgets import TinyMCE
from .models import *

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = '__all__'
        widgets = {
            'user' : forms.HiddenInput()
        }