from django.test import LiveServerTestCase, TestCase, tag
from django.test import Client
from django.urls import resolve
from django.urls import reverse
from selenium import webdriver

from . import views
from main.models import Pulau, Provinsi, Info, WebsitePemda, TelponPemda, EmailPemda
from datetime import datetime
# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class tambahDataUnitTest(TestCase):

    def test_url_response_status_200(self):
        response = self.client.get('/tambahData/')
        # self.assertEqual(response.status_code, 200)
        # response = self.client.get(reverse('tambahData:formTambah'))
        # self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/tambahData/')
        # self.assertTemplateUsed(response, 'tambahData/formTambah.html')

    def test_func_page(self):
        found = resolve('/tambahData/')
        self.assertEqual(found.func, views.formTambah)

    def test_kelengkapan_form_di_view(self):
        response = Client().get('/tambahData/')
        isi_html_kembalian = response.content.decode('utf8')  # fungsi decode('utf8') yaitu mendecode setiap string(yang umum dan tidak umum seperti kanji) menjadi huruf pada umumnya
        # self.assertIn("pulau", isi_html_kembalian)
        # self.assertIn("provinsi", isi_html_kembalian)
        # self.assertIn("protokol kesehatan", isi_html_kembalian)
        # self.assertIn("alamat website", isi_html_kembalian)
        # self.assertIn("email pemda", isi_html_kembalian)
        # self.assertIn("telpon", isi_html_kembalian)
        # self.assertIn("user", isi_html_kembalian)
        # #Memastikan setiap karakter terdapat di dalam halaman isi_html_kembalian
        # self.assertIn('button', isi_html_kembalian)
        #Memastikan di halaman /formulir/ terdapat elemen yang spesifik sesuai persyaratan

    def test_model_pulau(self):
        new_pulau = Pulau.objects.create(nama='jawa')

        counting_all_available_model = Pulau.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)
    
    def test_model_provinsi(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)

        counting_all_available_model = Provinsi.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_info(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')

        counting_all_available_model = Info.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_website_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_website_pemda = WebsitePemda.objects.create(info_id=new_info, alamat='www.google.com')

        counting_all_available_model = WebsitePemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_email_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_email_pemda = EmailPemda.objects.create(info_id=new_info, email='123@gmail.com')

        counting_all_available_model = EmailPemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_telpon_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_telpon_pemda = TelponPemda.objects.create(info_id=new_info, nomor_telpon='088188828883')

        counting_all_available_model = TelponPemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    # def test_model_tambah_data(self):
    #     new_data = Saran.objects.create(tanggal=datetime.today(), pulau = 'jawa', provinsi = 'jawa barat', info = '',
    #     website = 'jawabarat.go.id', no_telpon='082271660110', email='123@gmail.com')

    #     counting_all_available_model = tambahData.objects.all().count()
    #     self.assertEqual(counting_all_available_model, 1)

    