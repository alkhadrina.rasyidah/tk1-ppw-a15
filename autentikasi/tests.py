from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForAutentikasi(TestCase):

    def test_response_page(self):
        response = Client().get('/autentikasi/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_1(self):
        response = Client().get('/autentikasi/login/')
        self.assertTemplateUsed(response, 'autentikasi/login.html')

    def test_template_used_2(self):
        response = Client().get('/autentikasi/register/')
        self.assertTemplateUsed(response, 'autentikasi/register.html')

    def test_func_login(self):
        found = resolve('/autentikasi/login/')
        self.assertEqual(found.func, views.loginPage)

    def test_func_register(self):
        found = resolve('/autentikasi/register/')
        self.assertEqual(found.func, views.registerPage)

    def test_func_logout(self):
        found = resolve('/autentikasi/logout/')
        self.assertEqual(found.func, views.logoutUser)

    def test_func_autentikasi(self):
        found = resolve('/autentikasi/')
        self.assertEqual(found.func, views.autentikasi)

    def test_kelengkapan_formulir_di_login(self):
        response = Client().get('/autentikasi/login/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("LOGIN", isi_html_kembalian)
        self.assertIn("Username or email", isi_html_kembalian)
        self.assertIn("Password", isi_html_kembalian)

    def test_kelengkapan_formulir_di_register(self):
        response = Client().get('/autentikasi/register/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("REGISTER ACCOUNT", isi_html_kembalian)
        self.assertIn("Username..", isi_html_kembalian)
        self.assertIn("First name..", isi_html_kembalian)
        self.assertIn("Last name..", isi_html_kembalian)
        self.assertIn("Email..", isi_html_kembalian)
        self.assertIn("Enter password..", isi_html_kembalian)
        self.assertIn("Re-enter Password..", isi_html_kembalian)

        


