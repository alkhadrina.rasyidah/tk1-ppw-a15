from main.models import Saran
from django.shortcuts import render,redirect


from .forms import PostForm

# Create your views here.


def create(request):
    posts = Saran.objects.all()
    post_form = PostForm(request.POST or None)

    if request.method == 'POST' :#post request dari browser
        if post_form.is_valid():
            post_form.save()
            post_form = PostForm()
            return redirect('formSaran:create')
    context = {
        'page_title':'Saran',
        'post_form': post_form,
        'posts':posts,
    }
    return render(request,'formSaran/create.html',context)
