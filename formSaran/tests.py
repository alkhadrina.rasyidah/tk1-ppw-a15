# Create your tests here.
from django.test import TestCase, tag,Client
from django.urls import reverse,resolve
from selenium import webdriver
from .views import create
from main.models import Saran
from .forms import PostForm
from datetime import datetime, date

class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('formSaran:create'))
        self.assertEqual(response.status_code, 200)
    
    def test_dihalaman_formulir_ada_text_saran(self):
        response = self.client.get(reverse('formSaran:create'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Saran",html_kembalian)
        self.assertIn("Kirim",html_kembalian)
    
    def test_dihalaman_formulir_ada_template(self):
        response = self.client.get(reverse('formSaran:create'))
        self.assertTemplateUsed(response, 'formSaran/create.html')
    
    def test_model_saran(self):
        new_saran = Saran.objects.create(tanggal=datetime.today(), email='123@gmail.com', jenis_saran='P', saran='')
        counting_all_available_model = Saran.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_fungsi(self):
        func_saran = resolve('/formSaran/create/')
        self.assertEqual(func_saran.func, create)


    def test_model_bisa_dibuat_object_baru(self):
        # Creating a new saran
        new_saran = Saran.objects.create(tanggal=datetime.today(), email='123@gmail.com', jenis_saran='P', saran='')

        # Retrieving all available saran
        hitung_saran = Saran.objects.all().count()
        self.assertEqual(hitung_saran, 1)

    def test_form_validation_for_blank_items(self):
        form_form = PostForm(data={'email':'','jenis_saran':'','saran':''}) 

        self.assertFalse(form_form.is_valid())
        self.assertEqual(
            form_form.errors['email'],
            ["This field is required."]
        )
        self.assertEqual(
            form_form.errors['jenis_saran'],
            ["This field is required."]
        )
        self.assertEqual(
            form_form.errors['saran'],
            ["This field is required."]
                
        )

    def test_tkppw_post_success(self):
        test = 'Perbaikan website'
        response_post = Client().post('/formSaran/create/', {'email':test,'jenis_saran':test,'saran':test})
        self.assertEqual(response_post.status_code, 200)

        test2 = 'Masukkan data provinsi'
        
        response= Client().get('/formSaran/create/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        self.assertIn(test2, html_response)

    def test_tkppw_post_error(self):
        test = 'Anonymous'
        response_post = Client().post('/formSaran/create/', {'email':test,'jenis_saran':test,'saran':test})
        self.assertEqual(response_post.status_code, 200)

        test2 = 'Anonymous'

        response= Client().get('/formSaran/create/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
        self.assertNotIn(test2, html_response)

