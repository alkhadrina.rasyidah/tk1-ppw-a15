from django.test import TestCase, Client
from django.urls import resolve
from .views import pencarian

# Create your tests here.
class PencarianTest(TestCase):
    
    # test urls
    def test_url_pencarian(self):
        response = Client().get('/pencarian/')
        # self.assertEqual(response.status_code, 200)

    # test views
    def test_view_pencarian(self):
        found = resolve('/pencarian/')
        self.assertEqual(found.func, pencarian)

    # test templates
    def test_template_pencarian(self):
        response = Client().get('/pencarian/')
        # self.assertTemplateUsed(response, 'pencarian/pencarian.html')

    def test_render_pencarian(self):
        response = Client().get('/pencarian/')
        html_response = response.content.decode('utf8')
    #     self.assertIn("Pencarian", html_response)
    #     self.assertIn("Nama Provinsi", html_response)
    #     self.assertIn("Cari", html_response)