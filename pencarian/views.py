from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from main.models import *
from .forms import PencarianForm
from pencarian import forms
from main import models
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='autentikasi:login')
def pencarian(request):
    data_verified = VerifiedInfo.objects.all()
    form = PencarianForm(request.GET)
    context = {
        'form': PencarianForm()
    }
    if request.method == 'POST':
        query = str(request.POST['nama'])
        for info in data_verified:
            if str(info.info_id.nama_provinsi).lower() == query.lower():
                context['info'] = info.info_id
                return render(request, 'pencarian/pencarian.html', context)
    return render(request, 'pencarian/pencarian.html', context)