from django.db import models

# Create your models here.

# Province Data
class Pulau(models.Model):
    nama = models.CharField(max_length=35, primary_key=True)

    def __str__(self):
        return self.nama

class Provinsi(models.Model):
    nama = models.CharField(max_length=35, null=False)
    nama_pulau = models.ForeignKey(Pulau, on_delete=models.CASCADE, related_name='provinsi', null=False)

    def __str__(self):
        return self.nama

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['nama', 'nama_pulau'], name='composite primary key constraint provinsi')
        ]

class Info(models.Model):
    info_id = models.AutoField(primary_key=True)
    tanggal = models.DateField()
    nama_provinsi = models.ForeignKey(Provinsi, on_delete=models.CASCADE, related_name='info', null=False)
    protokol_kesehatan = models.TextField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['info_id', 'nama_provinsi'], name='composite primary key constraint info')
        ]

class WebsitePemda(models.Model):
    info_id = models.ForeignKey(Info, on_delete=models.CASCADE, related_name='website_pemda', null=False)
    alamat = models.URLField(null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['info_id', 'alamat'], name='composite primary key constraint website pemda')
        ]

class EmailPemda(models.Model):
    info_id = models.ForeignKey(Info, on_delete=models.CASCADE, related_name='email_pemda', null=False)
    email = models.EmailField(max_length=35, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['info_id', 'email'], name='composite primary key constraint email pemda')
        ]

class TelponPemda(models.Model):
    info_id = models.ForeignKey(Info, on_delete=models.CASCADE, related_name='telpon_pemda', null=False)
    nomor_telpon = models.CharField(max_length=20, null=False)
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['info_id', 'nomor_telpon'], name='composite primary key constraint telpon pemda')
        ]

class VerifiedInfo(models.Model):
    info_id = models.ForeignKey(Info, on_delete=models.CASCADE, related_name='verified', primary_key=True)

class NonVerifiedInfo(models.Model):
    info_id = models.ForeignKey(Info, on_delete=models.CASCADE, related_name='non_verified', null=False)
    email = models.CharField(max_length=35, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['info_id', 'email'], name='composite primary key constraint non verified info')
        ]

# Feedback Data
class Saran(models.Model):
    SARAN_CHOICES = (
        ('Perbaikan website', 'Perbaikan website'),
        ('Masukkan data provinsi', 'Masukkan data provinsi')
    )

    saran_id = models.AutoField(primary_key=True)
    tanggal = models.DateField(auto_now_add = False, auto_now = True) #baru
    email = models.EmailField(max_length=35)
    jenis_saran = models.CharField(max_length=25, choices=SARAN_CHOICES)
    saran = models.TextField()