from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from main.models import *
from datetime import datetime

class StatusTest(TestCase):

    def test_status_urls_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)
    
    def test_status_using_home_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response , 'status/home.html')

    def test_form_status_using_home_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func , home)

    def test_form_status_can_be_use(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        response = Client().post('/status/',{'nama_pulau':'Jawa'})

    def test_detail_urls_exist(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        response = Client().get('/status/Jawa/')
        self.assertEqual(response.status_code, 200)
    
    def test_detail_using_detail_template(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        response = Client().get('/status/Jawa/')
        self.assertTemplateUsed(response , 'status/detail.html')

    def test_detail_using_detail_func(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        found = resolve('/status/{pulau}/'.format(pulau='Jawa'))
        self.assertEqual(found.func , detail)

    def test_detail_prov_urls_exist(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        obj2 = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=obj1)
        tanggal = datetime.today()
        pulau = obj1
        provinsi = obj2
        info = Info.objects.create(tanggal = tanggal, nama_provinsi = provinsi, protokol_kesehatan = 'info_protokol')
        website = WebsitePemda.objects.create(info_id = info, alamat = 'https://pikobar.jabarprov.go.id/')
        no_telp = TelponPemda.objects.create(info_id = info, nomor_telpon = '021877212')
        pemda = EmailPemda.objects.create(info_id = info, email = 'alkhadrinazahra@gmail.com')
        non_verified = NonVerifiedInfo.objects.create(info_id = info, email = 'alkhadrinazahra@gmail.com')
        name = obj2.nama
        name = name.replace(' ','')
        name = name[:4]+"%20"+name[4:]
        response = Client().get('/status/Jawa/{prov}/'.format(prov=name))
        # self.assertEqual(response.status_code, 200)
    
    def test_detail_prov_using_detail_prov_template(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        obj2 = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=obj1)
        tanggal = datetime.today()
        pulau = obj1
        provinsi = obj2
        info = Info.objects.create(tanggal = tanggal, nama_provinsi = provinsi, protokol_kesehatan = 'info_protokol')
        website = WebsitePemda.objects.create(info_id = info, alamat = 'https://pikobar.jabarprov.go.id/')
        no_telp = TelponPemda.objects.create(info_id = info, nomor_telpon = '021877212')
        pemda = EmailPemda.objects.create(info_id = info, email = 'alkhadrinazahra@gmail.com')
        non_verified = NonVerifiedInfo.objects.create(info_id = info, email = 'alkhadrinazahra@gmail.com')
        name = obj2.nama
        name = name.replace(' ','')
        name = name[:4]+"%20"+name[4:]
        response = Client().get('/status/Jawa/{prov}/'.format(prov=name))
        # self.assertTemplateUsed(response , 'status/detail_prov.html')

    def test_detail_prov_using_detail_prov_func(self):
        obj1 = Pulau.objects.create(nama='Jawa')
        obj2 = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=obj1)
        name = obj2.nama
        name = name.replace(' ','')
        name = name[:4]+"%20"+name[4:]
        found = resolve('/status/Jawa/{prov}/'.format(prov=name))
        self.assertEqual(found.func , detail_prov)